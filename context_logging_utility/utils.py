import copy
from logging import LoggerAdapter, getLogger, Logger
from logging.handlers import QueueHandler, QueueListener
from queue import Queue

from .constants import FAKE_LOGGER


try:
    from logging import getHandlerByName
except ImportError:
    from logging import _handlers

    def getHandlerByName(name: str):
        return _handlers.get(name)


def get_queue_handler(max_size=-1):
    queue = Queue(max_size)
    handler = QueueHandler(queue)

    return handler


def contextualize(logger, **kwargs) -> LoggerAdapter:
    return LoggerAdapter(logger, extra=copy.deepcopy(kwargs))


def _is_queue_handler(handler):
    return isinstance(handler, QueueHandler)


def get_queue_by_logger(logger: Logger | str) -> Queue:
    if isinstance(logger, str):
        logger = getLogger(logger)

    queue_handlers = filter(_is_queue_handler, logger.handlers)
    queue_handler: QueueHandler = next(queue_handlers)

    return queue_handler.queue


def create_listener(default_logger: Logger | str = "root"):
    fake_queue_logger = getLogger(FAKE_LOGGER)
    queue = get_queue_by_logger(default_logger)

    listener = QueueListener(queue, *fake_queue_logger.handlers)
    return listener
